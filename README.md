# FootBall API v2 - By Ben Elany
FootBall API widget based on Spring Boot + React

## Deployment instructions:
1. Install Java 8 and Maven 3+
2. Go to the root folder
3. Run in cmd: mvn clean package
4. Then go to /backend/target/
5. Your artifact is 'backend-1.0-SNAPSHOT.jar'

## If you need to run it locally: 
1. run in cmd: java -jar backend-1.0-SNAPSHOT.jar
2. go to localhost:8080
3. Profit! 

### How to import the project to Intelij from Bitbucket:
1. VCS -> Checkout from version control -> Git
2. Copy the http url from Bitbucket ( Click "Clone" on the right upper corner)
3. Then Clone

## Additional step: 
Go to Settings -> Plugins -> Install Lombok plugin

## Deployment strategy:
### Each time you commit and push to master branch in bitbucket:
1. Bitbucket pipeline handles the building, compiling and creating the artifacts.
2. Bitbucket pipeline deploys to production environment if the build succeeded.  
3. Bitbucket pipeline uploads the artifact to AWS BeanStalk, restart it with a new version, and saves the old version in dedicated s3 bucket.