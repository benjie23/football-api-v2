package com.benjie.football;


import com.benjie.football.model.Area;
import com.benjie.football.model.Competition;
import com.benjie.football.model.Standing;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller that exposes REST API that can be consumed by the frontend
 */
@CrossOrigin (origins = "*", allowedHeaders = "*")
@RestController
public class ApiController {

  private final FootballApiService footballApiService;

  public ApiController(FootballApiService footballApiService) {
    this.footballApiService = footballApiService;
  }

  @GetMapping("/areas")
  public List<Area> getAreas() {
    return footballApiService.fetchAreas();
  }

  @GetMapping("/competitions/{areaId}")
  public List<Competition> getCompetitionsByArea(@PathVariable String areaId) {
    return footballApiService.fetchCompetitionsByArea(areaId);
  }

  @GetMapping("/standings/{competitionId}")
  public List<Standing> getStandingsByAreaAndCompetition(@PathVariable String competitionId) {
    return footballApiService.fetchStandingsByCompetition(competitionId);
  }
}
