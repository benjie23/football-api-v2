package com.benjie.football;

import com.benjie.football.model.Area;
import com.benjie.football.model.Competition;
import com.benjie.football.model.Standing;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * This service is needed to decouple outer API from the footbal-data.org
 */
@Service
@Slf4j
public class FootballApiService {

  private final RestTemplate restTemplate;

  @Value("${footballapi_token}")
  private String footballApiToken;

  private final static String AREAS_URL = "http://api.football-data.org/v2/areas/";
  private final static String COMPETITION_URL_TEMPLATE = "http://api.football-data.org/v2/competitions?areas=%s";
  private final static String STANDINGS_URL_TEMPLATE = "http://api.football-data.org/v2/competitions/%s/standings";

  private final LoadingCache<String, List<Area>> areasCache;
  private final LoadingCache<String, List<Competition>> competitionsCache;
  private final LoadingCache<String, List<Standing>> standingsCache;

//Constructor
  public FootballApiService(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;

    areasCache = CacheBuilder.newBuilder()
        .expireAfterAccess(24, TimeUnit.HOURS)
        .build(new CacheLoader<String, List<Area>>() {
          @Override
          public List<Area> load(String key) {
            return new ArrayList<>();
          }
        });
    competitionsCache = CacheBuilder.newBuilder()
        .expireAfterAccess(10, TimeUnit.MINUTES)
        .build(new CacheLoader<String, List<Competition>>() {
          @Override
          public List<Competition> load(String key) {
            return new ArrayList<>();
          }
        });
    standingsCache = CacheBuilder.newBuilder()
        .expireAfterAccess(10, TimeUnit.MINUTES)
        .build(new CacheLoader<String, List<Standing>>() {
          @Override
          public List<Standing> load(String key) {
            return new ArrayList<>();
          }
        });
  }

  private JsonNode makeCall(URI url) throws IOException {
    ResponseEntity<String> responseEntity = restTemplate
            .exchange(RequestEntity.get(url)
                    .header("X-Auth-Token", footballApiToken)
                    .build(), String.class);
    //Helps us read the json, and extract what we need from it.
    ObjectMapper mapper = new ObjectMapper();
    JsonNode rootNode = mapper.readTree(responseEntity.getBody());
    return rootNode;

  }
  public List<Area> fetchAreas() {
    URI url = URI.create(AREAS_URL);
    try {
      JsonNode rootNode = this.makeCall(url);
      ArrayNode areasNode = (ArrayNode) rootNode.path("areas");
      List<Area> areas = new ArrayList<>();
      areasNode.elements().forEachRemaining(node -> areas
          .add(new Area(node.get("id").asText(), node.get("name").asText())));

      areasCache.put("all", areas);
    } catch (HttpStatusCodeException | IOException e) {
      log.info("{} : {}, returning results from cache if present", url, e.getLocalizedMessage());
    }

    return areasCache.getUnchecked("all");
  }

  public List<Competition> fetchCompetitionsByArea(String id) {
    URI url = URI.create(String.format(COMPETITION_URL_TEMPLATE, id));
    try {
      JsonNode rootNode = this.makeCall(url);
      ArrayNode competitionsNode = (ArrayNode) rootNode.path("competitions");
      //filtering the data
      List<Competition> competitions = new ArrayList<>();
      competitionsNode.elements().forEachRemaining(node -> competitions
          .add(new Competition(node.get("id").asText(),
              node.get("name").asText())));

      competitionsCache.put(id, competitions);
    } catch (HttpStatusCodeException | IOException e) {
      log.info("{} : {}, previous results returned", url, e.getLocalizedMessage());
    }

    return competitionsCache.getUnchecked(id);
  }

  public List<Standing> fetchStandingsByCompetition(String id) {
    URI url = URI.create(String.format(STANDINGS_URL_TEMPLATE, id));
    try {
      JsonNode rootNode = this.makeCall(url);
      ArrayNode tableNode = (ArrayNode) rootNode.path("standings").get(0).get("table");

      List<Standing> standings = new ArrayList<>();
      tableNode.elements().forEachRemaining(node -> standings
          .add(Standing.builder()
              .position(node.get("position").asInt())
              .teamName(node.get("team").get("name").asText())
              .playedGames(node.get("playedGames").asInt())
              .won(node.get("won").asInt())
              .draw(node.get("draw").asInt())
              .lost(node.get("lost").asInt())
              .points(node.get("points").asInt())
              .build()));

      standingsCache.put(id, standings);
    } catch (HttpStatusCodeException | IOException e) {
      log.info("{} : {}, previous results returned", url, e.getLocalizedMessage());
    }

    return standingsCache.getUnchecked(id);
  }
}

