package com.benjie.football;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * This Launcher is an entry point to start the java program.
 */
@SpringBootApplication
public class Launcher {

  public static void main(String[] args) {
    SpringApplication.run(Launcher.class, args);
  }


  @Bean
  public RestTemplate restTemplate() {
    return new RestTemplate();
  }

}
