package com.benjie.football.model;

import lombok.Builder;
import lombok.Getter;

/**
 * Model class that represents the Competition standings
 */
@Getter
@Builder
public class Standing {

  private int position;
  private String teamName;
  private int playedGames;
  private int won;
  private int draw;
  private int lost;
  private int points;
}
