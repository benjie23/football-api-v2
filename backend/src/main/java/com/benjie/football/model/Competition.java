package com.benjie.football.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Model class that represents the Competition
 */
@AllArgsConstructor
@Getter
public class Competition {

  private final String id;
  private final String name;
}
