package com.benjie.football.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Model class that represents the Area
 */
@AllArgsConstructor
@Getter
public class Area {

  private final String id;
  private final String name;
}
