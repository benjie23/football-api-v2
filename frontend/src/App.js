import React, { useState, useEffect } from 'react';
import './App.css';
import { makeStyles } from '@material-ui/core/styles';
import { 
  FormControl, 
  InputLabel, 
  Select, 
  Paper, 
  TableRow, 
  TableHead,
  TableCell, 
  TableBody, 
  Table,
  Button
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(3),
    overflowX: 'auto',
    marginBottom: theme.spacing(3)
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  table: {
    minWidth: 650,
  },
  none: {
    display: 'none'
  },
  error: {
    padding: 10,
    display: 'flex',
    justifyContent: 'center'
  }
}));

const API = {
  areas: '/areas',
  competitions: areaId => `/competitions/${areaId}`,
  standings: competitionId => `/standings/${competitionId}`
}

function App() {
  const classes = useStyles();
  const [areaState, setAreaState] = useState({
    area: '',
    areaList: [],
  });
  const [competitionState, setCompetitionState] = useState({
    competition: '',
    competitionList: [],
  })
  const [table, setTable] = useState([]);
  const [error, setError] = useState('');

  const changeArea = async (event) => {
    setTable([])
    setError('')
    setCompetitionState({
      competition: '',
      competitionList: []
    })
    setAreaState({
      ...areaState,
      area: event.target.value
    });
    const result = await fetch(API.competitions(event.target.value)).then(data => data.json());
    setCompetitionState({
      ...competitionState,
      competitionList: result,
    })
  }

  const changeCompetition = (event) => {
    setError('')
    setCompetitionState({
      ...competitionState,
      competition: event.target.value,
    })
  }

  const getStandings = async () => {
    setTable([])
    setError('')
    let result = await fetch(API.standings(competitionState.competition)).then(data => data.json());
    setTable(result)
    if (result.length === 0) {
      setError('You need to upgrade to see this content')
    }
  }

  useEffect(() => {
      const fetchData = async () => {
      const result = await fetch(API.areas).then(data => data.json());
      setAreaState({...areaState, areaList: result});
    };
    fetchData();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  return (
    <div className="container">
      <div className="table">
        <div className="buttons">
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="sltArea">Area</InputLabel>
            <Select
              native
              value={areaState.area}
              onChange={changeArea}
              inputProps={{
                name: 'area',
                id: 'sltArea',
              }}
            >
              <option value=""/>
              {
                areaState.areaList.map(area => <option key={area.id} value={area.id}>{area.name}</option>)
              }
            </Select>
          </FormControl>
          <FormControl className={classes.formControl} disabled={!areaState.area}>
            <InputLabel htmlFor="sltCompetition">Competition</InputLabel>
            <Select
              native
              value={competitionState.competition}
              onChange={changeCompetition}
              inputProps={{
                name: 'competition',
                id: 'sltCompetition',
              }}
            >
              <option value="" />
              {
                competitionState.competitionList.map(area => <option key={area.id} value={area.id}>{area.name}</option>)
              }
            </Select>
          </FormControl>
          <Button 
            variant="contained" 
            color="primary"
            className={classes.formControl}
            disabled={!areaState.area || !competitionState.competition}
            onClick={getStandings}
          >
          GET
          </Button>
        </div>
        <Paper className={classes.root}>
          <Table className={classes.table}>
            <TableHead>
              <TableRow>
                <TableCell>Position</TableCell>
                <TableCell align="right">Team Name</TableCell>
                <TableCell align="right">Played Games</TableCell>
                <TableCell align="right">Won</TableCell>
                <TableCell align="right">Draw</TableCell>
                <TableCell align="right">Lost</TableCell>
                <TableCell align="right">Points</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {table.map(row => (
                <TableRow key={row.teamName}>
                  <TableCell component="th" scope="row">
                    {row.position}
                  </TableCell>
                  <TableCell align="right">{row.teamName}</TableCell>
                  <TableCell align="right">{row.playedGames}</TableCell>
                  <TableCell align="right">{row.won}</TableCell>
                  <TableCell align="right">{row.draw}</TableCell>
                  <TableCell align="right">{row.lost}</TableCell>
                  <TableCell align="right">{row.points}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
          { error && <span className={classes.error}>{error}</span> }
        </Paper>
      </div>
    </div>
  );
}

export default App;
